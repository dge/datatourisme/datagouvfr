#!/bin/bash
cd "$(dirname "$0")"

source _env.sh

curl -X POST ${SPARQL_ENDPOINT}?GETSTMTS > "${DATASET_DUMP_PATH}"

#zip dataset
tfile=$(mktemp -u)
zip -q "${tfile}" "${DATASET_DUMP_PATH}"

#date=$(date +%Y%m%d_%H%M)
date=$(date +%Y%m%d)

curl -s -X POST -H "Accept: application/json" \
 	-H "X-API-KEY: ${DATAGOUV_APIKEY}" \
    -F "file=@${tfile};filename=datatourisme.${date}.nt.zip" \
    "https://www.data.gouv.fr/api/1/datasets/${DATAGOUV_DATASET}/upload/" > "logs/dataset.log"

rm $tfile	
