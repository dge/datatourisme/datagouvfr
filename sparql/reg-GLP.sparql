#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier: GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

prefix schema: <http://schema.org/>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix hint: <http://www.bigdata.com/queryHints#>

select

	(?label as ?Nom_du_POI)
	(?rdfsTypes as ?Categories_de_POI)
	(?latitude as ?Latitude)
	(?longitude as ?Longitude)
	(?streetAddress as ?Adresse_postale)
	(?citys as ?Code_postal_et_commune)
	(?periods as ?Periodes_regroupees)
  ?Covid19_mesures_specifiques
  ?Covid19_est_en_activite
  ?Covid19_periodes_d_ouvertures_confirmees
	(?creatorLabel as ?Createur_de_la_donnee)
	(?publisherLabel as ?SIT_diffuseur)
	(?lastUpdate as ?Date_de_mise_a_jour)
	(?contacts as ?Contacts_du_POI)
  (?classements as ?Classements_du_POI)
	(?comments as ?Description)
	(?idUri as ?URI_ID_du_POI)
	

where {
      hint:Query hint:optimizer "None" .
  #bind(<https://data.datatourisme.gouv.fr/46/37127949-743f-3897-bfc2-deeca68e3680> as ?idUri)

    {select ?idUri (group_concat(?rdfsType;SEPARATOR="|") as ?rdfsTypes) (sample(?label) as ?label) (sample(?periods) as ?periods) where {

        {select  ?idUri ?label ?periods where {
	#?idUri a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest>.
          {select ?idUri (group_concat(?dates; separator="|") as ?periods) where {
                ?idUri a <https://www.datatourisme.gouv.fr/ontology/core#EntertainmentAndEvent>.
            	?idUri <https://www.datatourisme.gouv.fr/ontology/core#isLocatedAt> ?place.
                ?place <http://schema.org/geo> ?geo ; <http://schema.org/address> ?adr.
            	?adr  <https://www.datatourisme.gouv.fr/ontology/core#hasAddressCity> ?city.
                ?city <https://www.datatourisme.gouv.fr/ontology/core#isPartOfDepartment>/<https://www.datatourisme.gouv.fr/ontology/core#isPartOfRegion> <https://www.datatourisme.gouv.fr/resource/core#France01>.
                ?idUri <https://www.datatourisme.gouv.fr/ontology/core#takesPlaceAt> ?period.
                ?period <https://www.datatourisme.gouv.fr/ontology/core#startDate> ?startDate ; <https://www.datatourisme.gouv.fr/ontology/core#endDate> ?endDate.
                bind (concat(str(?startDate),"<->",str(?endDate)) as ?dates)
            } group by ?idUri
           } 
            union 
           {select ?idUri where {
             	?idUri a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest>.
                ?idUri <https://www.datatourisme.gouv.fr/ontology/core#isLocatedAt> ?place.
                ?place <http://schema.org/geo> ?geo ; <http://schema.org/address> ?adr.
            	?adr  <https://www.datatourisme.gouv.fr/ontology/core#hasAddressCity> ?city.
                ?city <https://www.datatourisme.gouv.fr/ontology/core#isPartOfDepartment>/<https://www.datatourisme.gouv.fr/ontology/core#isPartOfRegion> <https://www.datatourisme.gouv.fr/resource/core#France01>.
                FILTER NOT EXISTS { ?idUri a <https://www.datatourisme.gouv.fr/ontology/core#EntertainmentAndEvent>. }
                               }
           }
           ?idUri rdfs:label ?label.
           FILTER langMatches( lang(?label), "fr" )
         }
        }
       ?idUri a ?rdfsType .
      } group by ?idUri
     }   

  	?idUri <https://www.datatourisme.gouv.fr/ontology/core#isLocatedAt> ?place.
    ?place <http://schema.org/address> ?adr.
  
    {select ?adr (group_concat(?cityTmp;SEPARATOR="|") as ?citys)
        where {
            {select ?adr (concat(?pcode,"#",?cityLabel) as ?cityTmp)
            where {
                ?adr  <https://www.datatourisme.gouv.fr/ontology/core#hasAddressCity> ?city.
                ?adr  <http://schema.org/postalCode> ?pcode.
                ?city <https://www.datatourisme.gouv.fr/ontology/core#isPartOfDepartment>/<https://www.datatourisme.gouv.fr/ontology/core#isPartOfRegion> <https://www.datatourisme.gouv.fr/resource/core#France01>.
                ?city rdfs:label ?cityLabel.
                FILTER langMatches( lang(?cityLabel), "fr" )
             }
            }
        } group by ?adr
    }
    ?place <http://schema.org/geo> ?geo .
	?geo <http://schema.org/longitude> ?longitude ; <http://schema.org/latitude> ?latitude.
	?idUri <https://www.datatourisme.gouv.fr/ontology/core#lastUpdate> ?lastUpdate
	; <https://www.datatourisme.gouv.fr/ontology/core#hasBeenPublishedBy> ?publisher
	; <https://www.datatourisme.gouv.fr/ontology/core#hasBeenCreatedBy> ?creator.
  	?creator <http://schema.org/legalName> ?creatorLabel.
	?publisher <http://schema.org/legalName> ?publisherLabel.
  
  
    optional {
        ?idUri <https://www.datatourisme.gouv.fr/ontology/core#COVID19SpecialMeasures> ?Covid19_mesures_specifiques.
        FILTER langMatches( lang(?Covid19_mesures_specifiques), "fr" )
    }
    optional {
        ?idUri <https://www.datatourisme.gouv.fr/ontology/core#COVID19InOperationConfirmed> ?Covid19_est_en_activite.
    }
    optional {
        ?idUri <https://www.datatourisme.gouv.fr/ontology/core#COVID19OpeningPeriodsConfirmed> ?Covid19_periodes_d_ouvertures_confirmees.
    }


        
    optional {
        {select ?idUri (group_concat(?contact;SEPARATOR="|") as ?contacts) where {
        ?idUri <https://www.datatourisme.gouv.fr/ontology/core#hasContact> ?contactUri.
        {select ?contactUri (concat(group_concat(distinct ?cName;SEPARATOR="<>"),"#",group_concat(distinct ?cPhone;SEPARATOR="<>"),"#",group_concat(distinct ?cEmail;SEPARATOR="<>"),"#",group_concat(distinct ?cSite;SEPARATOR="<>")) as ?contact) where {
            ?contactUri a <https://www.datatourisme.gouv.fr/ontology/core#Agent>.
            optional {
            ?contactUri schema:email ?cEmail.
            }
            optional {
            ?contactUri schema:legalName ?cName.
            }
            optional {
            ?contactUri schema:telephone ?cPhone.
            }
            optional {
            ?contactUri foaf:homepage ?cSite.
            }          
            } group by ?contactUri
            }
        } group by ?idUri      
        }
    }

    optional {
        {select ?idUri (group_concat(?comment; separator="<>") as ?comments) where {
        ?idUri rdfs:comment ?comment.
        filter langMatches( lang(?comment), "fr" )
        } group by ?idUri
        }
    }
    
    optional {
        {select ?idUri (group_concat(distinct ?classement;SEPARATOR="|") as ?classements) where {
        ?idUri <https://www.datatourisme.gouv.fr/ontology/core#hasReview> ?review.
        {select distinct ?review (concat(?rValue,"#",?cValue) as ?classement) where {
            ?review a <https://www.datatourisme.gouv.fr/ontology/core#Review>.        
            ?review <https://www.datatourisme.gouv.fr/ontology/core#hasReviewValue> ?rv. ?rv rdfs:label ?rValue.
            ?reviewet <https://www.datatourisme.gouv.fr/ontology/core#allowsRating> ?rv; rdfs:label ?cValue.
            filter langMatches( lang(?rValue), "fr" )
            filter langMatches( lang(?cValue), "fr" )                
            }               
        }
    } group by ?idUri      
    }
    }
        
    optional {
        {select ?adr (group_concat(?sa;SEPARATOR=" ") as ?streetAddress) where { ?adr <http://schema.org/streetAddress> ?sa .} group by ?adr}
    }
  
}