CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.0] - 2022-01-27

### Ajouts
- publication initiale